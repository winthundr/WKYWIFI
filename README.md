# 最便宜APRS网关（不到100元）制作教程

![输入图片说明](wky%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20230331100520.jpg)

## 介绍

仅仅用玩客云+电视棒，成本不到100元即可玩转APRS网关。

学习BG9EGA的教程制作，后又整合支持无线网卡的系统，完善后可以使用大部分常用USB无线网卡，摆脱了网线束缚。

BG9EGA大佬的玩客云做APRS网关教程：[APRS折腾之：IGate网关](https://bg9ega.cn/290.html)

为了摆脱网线找到的支持无线网卡的系统：[玩客云Armbian_20.11_Aml-s812_5.9.0-支持USB-WIFI最终完美版.img.xz](https://www.right.com.cn/FORUM/forum.php?mod=viewthread&tid=4103842&page=1&extra=#pid11108017)

#### 确定支持的网卡：360随身wifi 1代(RT5370芯片)  360随身wifi 2代 (MT7601U芯片)  小度WiFi (MT7601芯片)  磊科NW360(RTL8191SU芯片)  Tenda U1（RTL8192EU芯片）  水星MW150UH（RTL8188EUS芯片）
#### 刷机包下载链接：
百度网盘：[https://pan.baidu.com/s/1douaDmA0i1tqeOHDpMaj6A](https://pan.baidu.com/s/1douaDmA0i1tqeOHDpMaj6A) 提取码: aprs

天翼云盘：https://cloud.189.cn/t/b6Jfiy6jqe6r（访问码：31jo）

# 制作步骤

## 一、玩客云刷系统：（账号 root 密码 1234）

●●●注意：只刷5.88系统可用网线连接路由，如果想使用USB无线网卡，需要先刷5.88系统，再刷5.9系统，不能直接刷5.9系统。●●●

 **1.  刷底包：** 拆机短接，刷入首选底包.img（已经刷过armbian系统的跳过本步骤）

请参考刷底包视频，2分50秒之前内容：
[拆机刷底包视频](https://www.bilibili.com/video/BV1Jg411Y77e/?spm_id_from=333.337.search-card.all.click&vd_source=9ca1fcd86b9d83c6d2f5b08832b6ad0f)

老主板短接点：

![输入图片说明](image222.png)

新主板短接点：

![输入图片说明](image.png) 

 **2.  刷入armbian5.88系统：** 用 “U盘写入工具” 把镜像 “armbian5.88.img” 写入U盘，插入离网口较近的那个usb口，然后玩客云通电，等待 **蓝灯长亮** 表示写入完毕。（时间较长，耐心等待）

 **3.  刷入armbian5.9系统：** 拔出U盘，把 “armbian5.9.img” 镜像写入到U盘,插到离网口较近的那个usb口， **插入网线** ，然后通电。 **蓝灯常亮** 代表写入完毕。

 **4.  找到IP并连接：** 路由器中找到玩客云的IP（名为arm的设备）。用putty连接此IP进入armbian（如果连不上就多等一会再试，刚出来IP就是连不上）。

 **5.  登录：** 连接后用用户名“root”和密码“1234”登入系统（输密码的时候不会显示）

 **6.  安装5.9系统到机身：**
依次输入
```bash
cd /boot/install
```
```bash
./install.sh
```
此时开始安装，当提示【Complete copy OS to eMMC】说明系统安装完毕。

拔掉U盘，插入usb网卡，重新上电。（不要拔掉网线）

## 二、WIFI设置

1.  用putty连接进入armbian后，输入命令：nmtui 回车
2.  选择第二项，找到要连的wifi，输入密码，然后退出。

![输入图片说明](image3.png)

![输入图片说明](image4.png)

3.  现在可以拔掉网线了，重新断电上电。进入路由，找到新的usb网卡的ip地址。

## 三、安装Direwolf：（如果怕输错就一行一行输入）（提示输入Y/N一律输入Y回车就行了）

1.  用putty连接进入armbian，更新软件包列表并安装编译所需环境。
```bash
apt-get update
apt-get install git build-essential cmake automake libasound2-dev libudev-dev alsa-utils
```
2.  安装RTL-SDR软件（Debian系统源里有rtl-sdr软件包，这里就不用编译了，直接安装即可。）
```bash
apt-get install libusb-1.0-0-dev rtl-sdr
```
3.  拉取Direwolf软件源代码并编译
```bash
cd ~
git clone https://gitee.com/creeper-H/direwolf
cd direwolf
git checkout dev
mkdir build && cd build
cmake ..
make –j4
```
4.  安装编译好的Direwolf
```bash
make install
```

## 四、设置配置文件

1.  创建配置文件
```bash
nano /root/direwolf.conf
```
把提前准备好的配置文件内容改好后粘贴进去，然后Ctrl+X按y保存退出

配置文件内容：

```bash
# （不用改）输入设备：监听UDP的7355端口，这样比较省资源且不会出现报错情况。
# （不用改）输出设备：因为APRS网关只接收信标，不发射，所以没有输出设备，null即是空。
ADEVICE udp:7355 null

# （改成自己的呼号）呼号及SSID，10代表互联网网关设备。
MYCALL BG9ABC-10

# （不用改）APRS-IS服务器使用国内的，就近原则。
IGSERVER china.aprs2.net

# （改成自己的呼号和呼号验证码）APRS-IS认证呼号-SSID及Passcode，可去22meters.com生成。
IGLOGIN BG9ABC-10 XXXXX

# 网关自身信标，让其他人知道你的APRS iGate处于工作状态。
# every=30 为自身信标上报间隔30分钟，lat和long为经纬度，comment为说明信息。delay=1意思是启动后延时1分钟发送。
PBEACON sendto=IG delay=1 every=30 symbol=/r lat=34^15.34N long=108^56.49E freq="144.640" comment="某某某APRS网关"

# 网关自定义信息。可定时发送自定义内容。
CBEACON sendto=IG delay=1:05 EVERY=60 INFO=">郑州数模中继（C4FM） 频率：439.175 -8 T88.5"
```
![输入图片说明](image5.png)



2. SDR频偏ppm值测量：
```bash
rtl_test -p
```  
建议测量15分钟以上使之达到一个稳定值。也可多次测

3. 编辑开机启动文件
```bash
nano /etc/rc.local
```
将下面的启动命令结尾添加“&”(后台运行的意思)然后加入到“exit 0”的前一行，如：

上一步测出来的频偏ppm值替换下面“17”这个值

```bash
rtl_fm -f 144.640M -p 17 - | direwolf -c /root/direwolf.conf -r 24000 -D 1 - &
exit 0
```
![输入图片说明](image6.png)

Ctrl+X按y保存退出

4.  赋予启动文件执行权限：
```bash
chmod +x /etc/rc.local
```




## 五、如果网络不稳经常掉线可以增加系统自动重启命令
1、通过SSH连接到您的Armbian设备。

2、输入以下命令以编辑Cron作业：
```bash
crontab -e
```
3、提示选择编辑器的话输入  1  ，回车

![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231206130421.png)

4、在编辑器中添加以下内容以设置每天执行重启系统的任务：
```bash
30 23 * * * /sbin/shutdown -r now
```
这个Cron表达式的含义是：在每天的23点30分执行 /sbin/shutdown -r now 命令来立即重启系统。

![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231205174548.png)

4、保存并关闭编辑器。在nano中，按下 Ctrl + X，然后输入 Y 以确认保存，最后按下 Enter。


